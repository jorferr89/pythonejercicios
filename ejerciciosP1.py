# EJERCICIOS EN PYTHON: https://pythondiario.com/2013/05/ejercicios-en-python-parte-1.html

"""
Definir una función max() que tome como argumento dos números y devuelva el mayor de ellos. 
"""
def maximo(num1, num2):
    if (num1 > num2):
        return num1
    else:
        return num2

# print(maximo(1000, 999))

"""
Definir una función max_de_tres(), que tome tres números como argumentos y devuelva el mayor de ellos.
"""

def maximo_3(num1, num2, num3):
    if ((num1 > num2) and (num1 > num3)):
        return num1
    elif ((num2 > num1) and (num2 > num3)):
        return num2
    elif ((num3 > num1) and (num3 > num2)):
        return num3

# print (maximo_3(800, 700, 699))

"""
Escribir una función que tome un carácter y devuelva True si es una vocal, de lo contrario devuelve False.
"""

def es_vocal(letra):
    vocales = ['a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U']
    if letra in vocales:
        return True 
    else:
        return False

# print (es_vocal('A'))

"""
Escribir una función sum() y una función multip() que sumen y multipliquen respectivamente todos los números 
de una lista. Por ejemplo: sum([1,2,3,4]) debería devolver 10 y multip([1,2,3,4]) debería devolver 24.
"""

def sumar(lista):
    #longitud = 0
    #longitud = len(lista)
    resultado = 0
    for i in lista:
        resultado = resultado + i
    print (resultado)

# sumar([1,2,3,4])

def multiplicar(lista):
    #longitud = 0
    #longitud = len(lista)
    resultado = lista[0]
    for i in lista:
        resultado = resultado * i
    print (resultado)

# multiplicar ([1,2,3,4])
    
"""
Definir una función inversa() que calcule la inversión de una cadena. Por ejemplo la cadena "estoy probando" 
debería devolver la cadena "odnaborp yotse"
"""

def inversa (cadena):
    longitud = 0
    cadena_invertida = str()
    # Obtengo la longitud de la cadena
    longitud = -(len(cadena)-1)
    # Recorrido inverso
    for n in range(longitud, 1):
        # Cambio a positivo
        n = abs(n)
        # Agrego las letras de manera invertida
        cadena_invertida += cadena[n]
    
    print (cadena_invertida)

# inversa('estoy probando')